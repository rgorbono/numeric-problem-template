package ch.cern.exercise;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

public class TryExp4j {

    public static void main(String[] args) {
        Expression expression = new ExpressionBuilder("2*3").build();
        System.out.println(expression.evaluate());
    }
}
