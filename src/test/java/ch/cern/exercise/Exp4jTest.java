package ch.cern.exercise;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

public class Exp4jTest {

    @Test
    void testExp4j() {
        Expression expression = new ExpressionBuilder("2*3").build();
        assertEquals(6, expression.evaluate());

    }
}
